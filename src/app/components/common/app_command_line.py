import urwid
from src.app.core.mode import store as mode_store, actions as mode_actions
from src.app.core.app_events import actions as app_actions


class AppCommandLine(urwid.Edit):
    def __init__(self):
        self._focus = False
        self.COMMAND_CAPTION = ':'
        self.SEARCH_CAPTION = '/'
        self.EMPTY_CAPTION = ''
        super().__init__(caption='')

    def render(self, size, focus=False):
        if self._focus == focus:
            return super(AppCommandLine, self).render(size, focus)
        self._focus = focus
        if focus is True:
            self.set_edit_text('')
            if mode_store.current_mode.value[1] not in (mode_store.COMMAND_MODE, mode_store.SEARCH_MODE):
                mode_actions.set_mode(mode_store.COMMAND_MODE)
                self.set_caption(self.COMMAND_CAPTION)
            if mode_store.current_mode.value[1] == mode_store.COMMAND_MODE:
                self.set_caption(self.COMMAND_CAPTION)
            elif mode_store.current_mode.value[1] == mode_store.SEARCH_MODE:
                self.set_caption(self.SEARCH_CAPTION)
        else:
            self.set_caption(self.EMPTY_CAPTION)
        return super(AppCommandLine, self).render(size, focus)

    def keypress(self, size, key):
        if key == 'enter':
            if self.get_edit_text() in ('q', 'Q', 'q!', 'Q!'):
                app_actions.exit_from_app()
            elif self.get_edit_text() == 'devtools':
                app_actions.toggle_dev_tools()
            mode_actions.set_mode(mode_store.NORMAL_MODE)
        return super(AppCommandLine, self).keypress(size, key)
