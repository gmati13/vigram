from . import app_status_bar, app_command_line, app_info_bar

command_line = app_command_line.AppCommandLine()
info_bar = app_info_bar.AppInfoBar()
status_bar = app_status_bar.AppStatusBar()