import urwid
from src.app.core.mode import store as mode_store
from src.app.core.app_events import events as app_events


class AppInfoBar(urwid.Columns):
    def __init__(self):
        self.current_mode = urwid.Text('')
        self.current_focus = urwid.Text('')
        self.info_message = urwid.Text('', align='right')
        super().__init__([
            (10, urwid.Padding(self.current_mode, left=1, right=0)),
            (15, urwid.Padding(self.current_focus, left=1, right=0)),
            urwid.Padding(self.info_message, left=1, right=1)
        ])
        mode_store.current_mode.subscribe(self.set_mode_label)
        mode_store.current_focus.subscribe(self.set_focus_label)
        app_events.state.subscribe(self.handle_app_actions)

    def set_mode_label(self, mode):
        self.current_mode.set_text('[{}]'.format(mode[1]))

    def set_focus_label(self, focus: str):
        self.current_focus.set_text('focus: {}'.format(focus.lower()))

    def show_warning(self, message):
        self.info_message.set_text('{}'.format(message))

    def handle_app_actions(self, action):
        if action == app_events.EXIT_FROM_APP_WITH_CTRL_C:
            self.show_warning('type <esc>:q<enter> to exit')

