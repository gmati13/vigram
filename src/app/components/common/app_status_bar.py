import urwid
from src.app.core.store.user_info import store as user_store
from src import utils

class AppStatusBar(urwid.Columns):
    def __init__(self):
        self.user_name = urwid.Text('loading...')
        super().__init__([
            (13, urwid.Padding(urwid.Text('legged in as'), left=1, right=0)),
            urwid.Padding(self.user_name, left=1, right=0)
        ])
        user_store.user.subscribe(lambda user: self.set_user_name(utils.users.get_fullname_or_title(user)))

    def set_user_name(self, user_name):
        self.user_name.set_text(user_name)
