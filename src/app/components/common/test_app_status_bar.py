import unittest
from pyrogram import User, Chat
from . import app_status_bar
from src.app.core.store.user_info import store as user_store

class TestAppStatusBar(unittest.TestCase):
    def setUp(self):
        self.status_bar = app_status_bar.AppStatusBar()

class TestDisplayingChatInfo(TestAppStatusBar):
    def test_should_be_an_empty_string(self):
        text, _ = self.status_bar.user_name.get_text()
        self.assertEqual(text, '')

    def test_should_display_user_name(self):
        user_store.user.on_next(User(id=1, first_name='Mati', last_name='Green'))
        text, _ = self.status_bar.user_name.get_text()
        self.assertEqual(text, 'Mati Green')

    def test_should_display_only_first_name(self):
        user_store.user.on_next(User(id=1, first_name='Mati'))
        text, _ = self.status_bar.user_name.get_text()
        self.assertEqual(text, 'Mati')

    def test_should_display_only_last_name(self):
        user_store.user.on_next(User(id=1, last_name='Green'))
        text, _ = self.status_bar.user_name.get_text()
        self.assertEqual(text, ' Green')

    def test_should_display_chat_title(self):
        user_store.user.on_next(Chat(id=1, title='Chat', type='chat'))
        text, _ = self.status_bar.user_name.get_text()
        self.assertEqual(text, 'Chat')
