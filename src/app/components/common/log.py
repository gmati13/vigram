import sys
import urwid
from datetime import datetime
from src.app.core.handlers import navigation

from src.app.core.app_events import events as app_events
import os


# class CustomOut:
#     def __init__(self, out, log):
#         self.log = log
#         self.out = out
#         self.old_out = getattr(sys, out)
#         self.flush = self.old_out.flush
#
#     def write(self, text: str):
#         try:
#             self.old_out(text)
#         except TypeError:
#             if text.strip() != '':
#                 self.log(self.out, text)

def make_log(text):
    return urwid.Text('[{time}]: {text}'.format(
        time=datetime.now().strftime('%H:%M:%S'),
        text=str(text)
    ))


class Log(urwid.ListBox):
    def __init__(self):
        # if os.environ.get('PY_ENV', 'development') != 'unittest':
        #     sys.stdout = CustomOut('stdout', self.log)
        #     sys.stderr = CustomOut('stdout', self.log)
        super().__init__(body=urwid.SimpleListWalker(contents=[]))
        app_events.state.subscribe(self.subscribe)
        self.info('logger initialized successfully')

    def log(self, text):
        self.body.contents.append(urwid.AttrMap(make_log(text), 'stdout', 'stdout-focus'))

    def info(self, text):
        self.body.contents.append(urwid.AttrMap(make_log(text), 'info', 'info-focus'))

    def error(self, text):
        self.body.contents.append(urwid.AttrMap(make_log(text), 'stderr', 'stderr-focus'))

    def keypress(self, size, key):
        [size, key] = navigation.bind_hjkl(self, size, key)
        return super().keypress(size, key)

    def subscribe(self, event):
        if isinstance(event, list):
            if event[0] == app_events.LOGGER_LOG:
                self.log(event[1])
            if event[0] == app_events.LOGGER_INFO:
                self.info(event[1])
            if event[0] == app_events.LOGGER_ERROR:
                self.error(event[1])
