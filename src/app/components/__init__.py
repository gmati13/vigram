import urwid
from .common import status_bar, command_line, info_bar
from .telegram import dialogs, chat
from .containers import app_columns, app_frame
from src.app.core.mode import store as mode_store
from src.app.core.mode import actions as mode_actions

chat_styled = urwid.AttrMap(chat, 'chat')
command_line_styled = urwid.AttrMap(urwid.Padding(command_line, left=1, right=1), 'input')
dialogs_styled = urwid.AttrMap(dialogs, 'dialogs')
info_bar_styled = urwid.AttrMap(info_bar, 'bar')
status_bar_styled = urwid.AttrMap(status_bar, 'bar')

columns = app_columns.AppColumns(
    widget_list=[
        (40, dialogs_styled),
        chat_styled
    ],
)

frame = app_frame.AppFrame(
    header=status_bar_styled,
    body=columns,
    footer=urwid.Pile(
        widget_list=[
            ('pack', info_bar_styled),
            ('pack', command_line_styled)
        ]
    )
)


def handle_focus_by_mode_change(mode):
    if mode[1] in (mode_store.COMMAND_MODE, mode_store.SEARCH_MODE):
        if frame.get_focus() != 'footer':
            frame.set_focus('footer')
    elif mode[1] == mode_store.INSERT_MODE:
        if frame.get_focus() != 'body':
            frame.set_focus('body')
            columns.set_focus(chat_styled)
            chat.set_focus('footer')
        else:
            if columns.get_focus() is chat_styled:
                chat.set_focus('footer')
            else:
                mode_actions.set_mode(mode_store.NORMAL_MODE)
    if mode[1] == mode_store.NORMAL_MODE:
        if frame.get_focus() != 'body':
            frame.set_focus('body')
        if columns.get_focus() is chat_styled:
            chat.set_focus('body')


mode_store.current_mode.subscribe(handle_focus_by_mode_change)
