import urwid
from src.app.core.store.chat_info import store as chat_store
from src.app.core.store.user_info import store as user_store
from src import utils

class AppChatBar(urwid.Columns):
    def __init__(self):
        self.chat_label = urwid.Text('')
        super().__init__([
            urwid.Padding(self.chat_label, left=1, right=0)
        ])
        chat_store.current_chat.subscribe(lambda chat: self.set_chat_label(utils.users.get_fullname_or_title(chat)))

    def set_chat_label(self, label):
        if (user_store.user.value is not None and chat_store.current_chat.value is not None
                and user_store.user.value['id'] == chat_store.current_chat.value['id']):
            self.chat_label.set_text(label + ' (me)')
        else:
            self.chat_label.set_text(label)
