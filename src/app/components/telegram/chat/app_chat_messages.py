import urwid
from src.app.core.store.messages import store as msg_store
from src.app.core.handlers import navigation
from src import utils

class AppChatMessages(urwid.ListBox):
    def __init__(self):
        self.messages_list = AppChatMessagesList()
        super().__init__(body=self.messages_list)

    def keypress(self, size, key):
        [size, key] = navigation.bind_hjkl(self, size, key)
        return super().keypress(size, key)

class AppChatMessagesList(urwid.SimpleListWalker):
    def __init__(self, **kwargs):
        super().__init__(**kwargs, contents=[])
        msg_store.chat_last_message.subscribe(self.add_new_message)
        msg_store.chat_history.subscribe(self.load_history)

    def load_history(self, messages):
        self.clear()
        for message in messages[::-1]:
            self.add_new_message(message)
        self.scroll_down()

    def add_new_message(self, message):
        self.append(urwid.AttrMap(urwid.Padding(AppChatMessage(message), left=1, right=1), None, 'message-focus'))
        self.scroll_down()

    def scroll_down(self):
        if len(self.contents) > 0:
            self.set_focus(len(self.contents) - 1)

class AppChatMessage(urwid.Pile):
    def __init__(self, message):
        super().__init__([
            urwid.Columns([
                urwid.AttrMap(
                    urwid.Text(utils.users.get_fullname_or_title(message['from_user'])),
                    'message-owner',
                ),
                urwid.AttrMap(
                    urwid.Text(str(utils.dates.get_time_string(message['date'])), align='right'),
                    'message-time',
                )
            ]),
            urwid.Text(str(message['text']))
        ])
