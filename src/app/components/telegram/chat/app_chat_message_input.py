import urwid
from src.app.core.app_events import actions as app_actions


class AppChatMessageInput(urwid.Edit):
    def __init__(self):
        super().__init__(caption='')

    def keypress(self, size, key):
        message_text = self.get_edit_text()

        if key == 'enter':
            if message_text.strip() != '':
                app_actions.send_message_to_current_chat(message_text)
                self.set_edit_text('')
        return super(AppChatMessageInput, self).keypress(size, key)
