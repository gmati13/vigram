import urwid
from src.app.core.mode import actions as mode_actions
from src.app.core.mode.store import CHAT_FOCUS
from .app_chat_messages import AppChatMessages
from .app_chat_bar import AppChatBar
from .app_chat_message_input import AppChatMessageInput


class AppChat(urwid.Frame):
    def __init__(self):
        self._focus = False
        self.chat_messages = AppChatMessages()
        self.chat_bar = AppChatBar()
        self.chat_message_input = AppChatMessageInput()

        super().__init__(
            body=self.chat_messages,
            footer=urwid.Pile([
                ('pack', urwid.AttrMap(self.chat_bar, 'bar')),
                ('pack', urwid.AttrMap(urwid.Padding(self.chat_message_input, left=1, right=1), 'input'))
            ])
        )

    def render(self, size, focus=False):
        if self._focus != focus and focus is True:
            mode_actions.set_focus(CHAT_FOCUS)
        self._focus = focus
        return super(AppChat, self).render(size, focus)
