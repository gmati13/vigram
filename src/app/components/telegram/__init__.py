from .chat import app_chat
from .dialogs import app_dialogs

chat = app_chat.AppChat()
dialogs = app_dialogs.AppDialogs()
