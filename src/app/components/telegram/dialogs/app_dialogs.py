import urwid
from src.app.core.store.dialogs import store as dialogs_store
from src.app.core.store.chat_info import store as chat_store
from src.app.core.mode import actions as mode_actions
from src.app.core.mode.store import DIALOGS_FOCUS
from src.app.core.handlers import navigation
from src.app.core.app_events import actions as app_actions
from src import utils

class AppDialogs(urwid.ListBox):
    def __init__(self):
        self._focus = False
        self.dialogs_list = AppDialogsList()
        super().__init__(body=self.dialogs_list)

    def render(self, size, focus=False):
        if self._focus != focus and focus is True:
            mode_actions.set_focus(DIALOGS_FOCUS)
        self._focus = focus
        return super(AppDialogs, self).render(size, focus)

    def keypress(self, size, key):
        if key in navigation.keys:
            [size, key] = navigation.bind_hjkl(self, size, key)
        elif key == 'enter':
            dialog = dialogs_store.dialogs.value[self.focus_position]
            chat_store.current_chat.on_next(dialog['chat'])
        return super().keypress(size, key)

class AppDialogsList(urwid.SimpleListWalker):
    def __init__(self):
        super().__init__([])
        dialogs_store.dialogs.subscribe(self.set_dialogs)

    def set_dialogs(self, dialogs):
        self.clear()
        for dialog in dialogs:
            self.append(urwid.AttrMap(urwid.Padding(AppDialog(dialog), left=1, right=1), None, 'dialog-focus'))

class AppDialog(urwid.Pile):
    def __init__(self, dialog):
        self._selectable = True
        self.dialog = dialog

        super().__init__([
            urwid.AttrMap(
                urwid.Text(utils.users.get_fullname_or_title(dialog['chat'])),
                'dialog-label', 'dialog-label-focus'
            ),
            urwid.Text(utils.emoji.de_emojify(
                str(dialog['top_message']['text'])[:38].replace('\n', ' ')
            )),
        ])
