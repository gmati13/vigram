from . import setup
import urwid
from src.app.core.mode.handler import handle_inputs
from src.app.core.app_events import events as app_events
from .components import frame
from .components.common.log import Log
from .styles import palette

logger = Log()
main_widget = urwid.Columns(widget_list=[frame])
app = urwid.MainLoop(
    widget=main_widget,
    palette=palette,
    unhandled_input=handle_inputs
)


def process_app_events(event):
    if event == app_events.TOGGLE_DEV_TOOLS:
        if len(main_widget.contents) > 1:
            main_widget.contents.pop()
        else:
            main_widget.widget_list.append(logger)


def bootstrap():
    app_events.state.subscribe(process_app_events)
    setup.configure(app)
    setup.bootstrap(app)
