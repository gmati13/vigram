__direction = {'j': 1, 'k': -1}
__map_keys = {'J': 'down', 'K': 'up', 'h': 'left', 'l': 'right'}
keys = list(__direction.keys()) + list(__map_keys.keys())

def bind_hjkl(self, size, key):
    focus_position = self.focus_position

    if key in __direction.keys():
        try: self.focus_position += __direction[key]
        except IndexError: self.focus_position = focus_position
    elif key in __map_keys.keys():
        key = __map_keys[key]

    return [size, key]
