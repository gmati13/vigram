from . import store
from src.app import app

def set_messages_to_current_chat(messages, force_render=True):
    store.chat_history.on_next(messages)
    if force_render:
        app.draw_screen()

def add_new_message_to_current_chat(message, force_render=True):
    store.chat_last_message.on_next(message)
    if force_render:
        app.draw_screen()
