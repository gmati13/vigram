from rx.subject import BehaviorSubject, Subject

chat_history = BehaviorSubject([])
chat_last_message = Subject()