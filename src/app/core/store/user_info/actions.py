from . import store
from src.app import app

def set_user(user, force_render=True):
    store.user.on_next(user)
    if force_render:
        app.draw_screen()