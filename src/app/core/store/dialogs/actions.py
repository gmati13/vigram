from . import store
from src.app import app

def set_dialogs(dialogs, force_render=True):
    store.dialogs.on_next(dialogs)
    if force_render:
        app.draw_screen()
