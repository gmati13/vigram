from . import store
from src.app import app

def set_current_chat(chat_or_user, force_render=True):
    store.current_chat.on_next(chat_or_user)
    if force_render:
        app.draw_screen()