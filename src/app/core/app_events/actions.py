from . import events


def emit_load_initial_data():
    events.state.on_next(events.LOAD_INITIAL_DATA)


def exit_from_app():
    events.state.on_next(events.EXIT_FROM_APP)


def force_rerender():
    events.state.on_next(events.FORCE_RERENDER)


def toggle_dev_tools():
    events.state.on_next(events.TOGGLE_DEV_TOOLS)


def throw_error_with_exit_by_ctrl_c():
    events.state.on_next(events.EXIT_FROM_APP_WITH_CTRL_C)


def logger_log(text):
    events.state.on_next([events.LOGGER_LOG, text])


def logger_info(text):
    events.state.on_next([events.LOGGER_INFO, text])


def logger_error(text):
    events.state.on_next([events.LOGGER_ERROR, text])


def send_message_to_current_chat(text):
    events.state.on_next([events.SEND_MESSAGE_TO_CURRENT_CHAT, text])
