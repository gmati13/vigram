from . import actions, store

map_keys = {
    'esc': lambda: actions.set_mode(store.NORMAL_MODE),
    'i': lambda: actions.set_mode(store.INSERT_MODE),
    ':': lambda: actions.set_mode(store.COMMAND_MODE),
    '/': lambda: actions.set_mode(store.SEARCH_MODE),
    'v': lambda: actions.set_mode(store.VISUAL_MODE),
}

def handle_inputs(unhandled_input):
    if unhandled_input in map_keys.keys():
        map_keys[unhandled_input]()