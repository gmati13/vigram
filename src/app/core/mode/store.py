from rx.subject import BehaviorSubject

VISUAL_MODE = 'VISUAL'
INSERT_MODE = 'INSERT'
COMMAND_MODE = 'COMMAND'
NORMAL_MODE = 'NORMAL'
SEARCH_MODE = 'SEARCH'

CHAT_FOCUS = 'CHAT'
DIALOGS_FOCUS = 'DIALOGS'

current_focus = BehaviorSubject(DIALOGS_FOCUS)
current_mode = BehaviorSubject((NORMAL_MODE, NORMAL_MODE))