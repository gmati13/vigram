from . import store

def set_focus(target_focus):
    if store.current_mode.value[1] in (store.SEARCH_MODE, store.COMMAND_MODE):
        set_mode(store.NORMAL_MODE)
    store.current_focus.on_next(target_focus)

def set_mode(target_mode):
    store.current_mode.on_next((store.current_mode.value[0], target_mode))