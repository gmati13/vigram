from src.app.core.app_events import actions

log = actions.logger_log
info = actions.logger_info
error = actions.logger_error
