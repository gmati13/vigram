import src.app.core.app_events.actions as app_actions
import src.app.core.app_events.events as app_events

def configure(app):
    app.screen.set_terminal_properties(colors=256)


def bootstrap(app):
    app.start()
    app.draw_screen()
    app_actions.emit_load_initial_data()
    app.event_loop.run()

    def process_app_events(event):
        if event == app_events.FORCE_RERENDER:
            app.draw_screen()

    app_events.state.subscribe(process_app_events)

