palette_dict = dict()

palette_dict['bar'] = {'fg': 'g70', 'bg': 'g35'}
palette_dict['dialogs'] = {'fg': 'g40', 'bg': 'g20'}
palette_dict['dialog-label'] = {'fs': 'bold', 'fg': 'g80', 'bg': palette_dict['dialogs']['bg']}
palette_dict['chat'] = {'fg': 'g50', 'bg': 'g10'}
palette_dict['message-owner'] = {'fg': 'g70', 'bg': 'g15'}
palette_dict['message-time'] = {'fg': 'g25', 'bg': palette_dict['message-owner']['bg']}
palette_dict['input'] = {'fg': 'g70', 'bg': 'g10'}
palette_dict['info'] = {'fg': '#aaf'}
palette_dict['stdout'] = {'fg': 'g80'}
palette_dict['stderr'] = {'fg': '#f00'}

focus_palette_dict = dict()

focus_palette_dict['dialog-focus'] = {'fg': 'g45', 'bg': 'g25'}
focus_palette_dict['dialog-label-focus'] = {
    'fs': palette_dict['dialog-label']['fs'],
    'fg': palette_dict['dialog-label']['fg'],
    'bg': focus_palette_dict['dialog-focus']['bg']
}
focus_palette_dict['message-focus'] = {'fg': 'g75', 'bg': palette_dict['message-owner']['bg']}
focus_palette_dict['info-focus'] = {'fg': palette_dict['info']['fg'], 'bg': 'g20'}
focus_palette_dict['stdout-focus'] = {'fg': palette_dict['stdout']['fg'], 'bg': 'g20'}
focus_palette_dict['stderr-focus'] = {'fg': palette_dict['stderr']['fg'], 'bg': 'g20'}


def map_attribute(name, attribute):
    return (
        name,
        attribute['fs'] if 'fs' in attribute else 'default',
        '', '',
        attribute['fg'] if 'fg' in attribute else 'default',
        attribute['bg'] if 'bg' in attribute else 'default'
    )


def map_palette(source_dict):
    return [map_attribute(k, v) for k, v in source_dict.items()]


palette = map_palette(palette_dict)
focus_palette = map_palette(focus_palette_dict)
