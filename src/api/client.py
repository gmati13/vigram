import sys
from pyrogram import Client

class __TgAPI(Client):
    def __init__(self):
        pass

    def configure(self, api_id, api_hash):
        super().__init__(
            'default',
            api_id,
            api_hash,
        )

    def connect(self, **args):
        stdout = sys.stdout
        sys.stdout = None
        result = super().connect(**args)
        sys.stdout = stdout
        return result


TgAPI = __TgAPI()