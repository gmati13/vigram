import os
import src.app.core.app_events.events as app_events
from src.app.core.store.messages import actions as msg_actions
from pyrogram import MessageHandler
from src.api.client import TgAPI
from src.api.handlers import on_message
from src.app.core.store.messages import actions as msg_actions
from src.app.core.store.dialogs import actions as dialogs_actions
from src.app.core.store.user_info import actions as user_actions
from src.app.core.store.chat_info import actions as chat_actions, store as chat_store
from src.app.core import logger
from src.app import app
from src import utils


def configure():
    TgAPI.configure(
        api_id=os.getenv('TG_API_ID'),
        api_hash=os.getenv('TG_API_HASH')
    )


def bootstrap():
    TgAPI.start()


def select_chat(chat):
    if chat is None:
        msg_actions.set_messages_to_current_chat(TgAPI.get_history('me'), force_render=False)
    else:
        msg_actions.set_messages_to_current_chat(TgAPI.get_history(chat['id']), force_render=True)


def load_initial_data():
    user = TgAPI.get_me()
    logger.info('logged in as {user}'.format(
        user=utils.users.get_fullname_or_title(user)
    ))
    user_actions.set_user(user, force_render=False)
    chat_actions.set_current_chat(user, force_render=False)
    dialogs_actions.set_dialogs(TgAPI.get_dialogs(pinned_only=True) + TgAPI.get_dialogs(), force_render=False)
    chat_store.current_chat.subscribe(select_chat)

    app.draw_screen()

    TgAPI.add_handler(MessageHandler(on_message.handler))


def handle_app_action(action):
    if action == app_events.LOAD_INITIAL_DATA:
        load_initial_data()
    if isinstance(action, list):
        if action[0] == app_events.SEND_MESSAGE_TO_CURRENT_CHAT:
            message_text = action[1]
            message = TgAPI.send_message(chat_store.current_chat.value['id'], message_text)
            msg_actions.add_new_message_to_current_chat(message, True)


app_events.state.subscribe(handle_app_action)
