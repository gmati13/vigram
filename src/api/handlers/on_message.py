from src.app.core.store.messages import actions as msg_actions
from src.app.core.store.chat_info import store as chat_store


def handler(client, message):
    if message['chat']['id'] == chat_store.current_chat.value['id']:
        msg_actions.add_new_message_to_current_chat(message, force_render=True)
