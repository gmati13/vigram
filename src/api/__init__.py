from src.api import setup

def bootstrap():
    setup.configure()
    setup.bootstrap()