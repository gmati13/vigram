from pyrogram import Chat
from . import emoji

def get_fullname_or_title(chat_or_user):
    label = ''

    if chat_or_user is None:
        return ''

    if isinstance(chat_or_user, Chat) and chat_or_user['title'] is not None:
        label = str(chat_or_user['title'])
    elif chat_or_user['first_name'] is not None:
        label = str(chat_or_user['first_name'])
    if chat_or_user['last_name'] is not None:
        label = label + ' ' + str(chat_or_user['last_name'])
    return emoji.de_emojify(label)