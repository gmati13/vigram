import re

def de_emojify(text):
    regrex_pattern = re.compile(pattern="["u"\U0001F000-\U0001FFFF""]+", flags=re.UNICODE)
    return regrex_pattern.sub(r'[emoji]', text)