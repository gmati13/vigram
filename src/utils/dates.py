import datetime as dt

def get_time_string(timestamp):
    return dt.datetime.fromtimestamp(timestamp).strftime('%H:%M:%S')