## ViGram

The application is Telegram client for command line based on vi-like interface.

![vigram preview](vigram-preview.png "ViGram")

requirements:
```sh
python v3.7
```

### Telegram API configuration

`.env.sample` contains an exaple of configuration.
`TG_API_ID` and `TG_API_HASH` are required variables.

```sh
cp .env.sample .env
vim .env
```

After edit your `.env` should looks like that:
```sh
export TG_API_ID=GD633YFHI8
export TG_API_HASH=FE5RGUGU7EVHGOY96EOYDKY3YDHCLH9
```

### virtualenv configuration:

create virtualenv:

```sh
python3.7 -m venv venv
```

activate virtualenv:
```sh
source venv/bin/activate
```

deactivate virtualenv:
```sh
deactivate
```

### start application

```sh
python main.py
```

### dependencies

install dependencies:

```sh
pip install -r requirements.txt
```

generate or update requirements.txt:

```sh
pip freeze > requirements.txt
```
