import signal
import sys
from src.api import bootstrap as core_bootstrap
from src.api.client import TgAPI
from src.app import bootstrap as app_bootstrap
from src.app import app
from src.app.core.app_events import actions as app_actions, events as app_events
from dotenv import load_dotenv

def exit_from_app():
    TgAPI.stop()
    app.stop()
    sys.exit(0)

def try_to_exit_with_ctrl_c():
    app_actions.throw_error_with_exit_by_ctrl_c()
    app.draw_screen()

if __name__ == '__main__':
    signal.signal(signal.SIGINT, lambda a, b: try_to_exit_with_ctrl_c())
    app_events.state.subscribe(lambda action: exit_from_app() if action == app_events.EXIT_FROM_APP else None)

    load_dotenv()
    core_bootstrap()
    app_bootstrap()
